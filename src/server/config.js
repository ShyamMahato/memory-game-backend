const mysql = require("mysql");
const path = require("path");

require("dotenv").config({ path: path.join(__dirname, "../../", ".env") });

const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB,
});

const startConnection = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject(err);
      } else {
        return resolve(connection);
      }
    });
  });
};

module.exports = { startConnection };
