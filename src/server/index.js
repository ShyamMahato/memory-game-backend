const express = require("express");
const path = require("path");
var cors = require("cors");

const createTable = require("./createTable");

const { startConnection } = require("../server/config");

const app = express();
app.use(cors());

app.use(express.static(path.join(__dirname, "../public")));

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use("/", require("../routes/game-info"));

app.use("*", (request, response) => {
  response.json({
    message: "404 Page Not Found.",
  });
});

const PORT = process.env.PORT;
const DB = process.env.DB;

app.listen(PORT, () => {
  console.log("Listening on port " + PORT);
  main();
});

async function main() {
  try {
    const connection = await startConnection();
    console.log("MySQL connection started.");

    const createPlayerRecordTableQuery = `CREATE TABLE IF NOT EXISTS ${DB}.player_record (id INT PRIMARY KEY AUTO_INCREMENT, player_name VARCHAR(100), level INT, score INT);`;

    await createTable(connection, createPlayerRecordTableQuery);

    console.log("Table created successfully.");
  } catch (error) {
    console.log(error);
  }
}
