const createTable = (connection, query) => {
  return new Promise((resolve, reject) => {
    connection.query(query, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(`Table created successfully.`);
      }
    });
  });
};

module.exports = createTable;
