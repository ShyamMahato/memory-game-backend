const express = require("express");
const router = express.Router();

const { startConnection } = require("../server/config");
const executeQuery = require("../server/executeQuery");

router.post("/", async (request, response) => {
  try {
    const connection = await startConnection();

    if (!request.body) {
      response.status(404).json({
        message: "Please fill all the data",
      });
    } else {
      const playerName = request.body.playerName;
      const level = request.body.level;
      const score = request.body.score;
      const insertQuery = `INSERT INTO ${process.env.DB}.player_record (player_name, level, score) VALUES("${playerName}", ${level}, ${score})`;
      const insertQueryResult = await executeQuery(connection, insertQuery);

      response.status(200).json({
        status: "success",
        playerId: insertQueryResult.insertId,
        message: "Data inserted successfully.",
      });
    }

    connection.release();
  } catch (error) {
    response.status(500).json({
      status: "failed",
      message: "Internal Server Error",
    });
  }
});

router.get("/scores", async (request, response) => {
  try {
    const connection = await startConnection();

    const getScoreQuery = `SELECT player_name, level, MAX(score) as score FROM ${process.env.DB}.player_record GROUP BY player_name, level ORDER BY score DESC`;
    const getScoreQueryResult = await executeQuery(connection, getScoreQuery);

    response.status(200).json({
      status: "success",
      data: getScoreQueryResult,
    });

    connection.release();
  } catch (error) {
    response.status(500).json({
      status: "failed",
      message: "Internal Server Error",
    });
  }
});

module.exports = router;
